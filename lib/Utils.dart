import 'package:flutter/material.dart';

const String colorWhite = "#ffffff";
const String colorBlack = "#000000";
const String colorHeaderBg = "#e4c66c";
const String colorGreen = "#34a853";
const String bgColor = "#b2b2b2";
const String arrowColor = "#A9A9A9";
const String transColor = "#ffffff00";

/*Android Live AD*/
/*const String androidBannerAd = "ca-app-pub-9477150650032250/6042937706";
const String androidFullScreenAd = "ca-app-pub-9477150650032250/3033630989";
const String androidVideoAd = "ca-app-pub-9477150650032250/6589732619";*/

/*Android Test AD*/
const String androidBannerAd = "ca-app-pub-3940256099942544/6300978111";
const String androidFullScreenAd = "ca-app-pub-3940256099942544/1033173712";
const String androidVideoAd = "ca-app-pub-3940256099942544/5224354917";

/*iOS Live AD*/
/*const String iOSBannerAd = "ca-app-pub-9477150650032250/7301206724";
const String iOSFullScreenAd = "ca-app-pub-9477150650032250/8313877826";
const String iOSVideoAd = "ca-app-pub-9477150650032250/1556897788";*/

/*iOS Test AD*/
const String iOSBannerAd = "ca-app-pub-3940256099942544/2934735716";
const String iOSFullScreenAd = "ca-app-pub-3940256099942544/4411468910";
const String iOSVideoAd = "ca-app-pub-3940256099942544/1712485313";

const String adAppID = "ca-app-pub-9477150650032250~5775344392";

String currentYear() {
  DateTime now = new DateTime.now();
  return now.year.toString();
}

Color getColorFromHex(String hexColor) {
  hexColor = hexColor.toUpperCase().replaceAll('#', '');

  if (hexColor.length == 6) {
    hexColor = 'FF' + hexColor;
  }

  return Color(int.parse(hexColor, radix: 16));
}
