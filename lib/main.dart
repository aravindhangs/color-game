import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'Utils.dart';

final firestoreInstance = Firestore.instance;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Color Game',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage();

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  BoxDecoration decoration0;
  BoxDecoration decoration1;
  BoxDecoration decoration2;
  BoxDecoration decoration3;

  @override
  Widget build(BuildContext context) {
    List loopTimes = new List();

    loopTimes.add(1);
    loopTimes.add(2);
    loopTimes.add(3);
    loopTimes.add(4);

    var rng = new Random();

    var levels_total = 4;
    var olderColor;
    var choosenColor;
    int points = 0;

    for (var level = 0; level <= levels_total; level++) {
      int i = 0;
      //array that stores the generated colors for each level
      List<String> generatedvalues = new List();
      while (i < level) {
        while (choosenColor == olderColor) {
          choosenColor = rng.nextInt(loopTimes.length);
        }
        //Thread
        /*Future.delayed(const Duration(milliseconds: 500), () {
          setState(() {
            // Here you can write your code for open new view
          });
        });*/

        olderColor = choosenColor;
        generatedvalues.add(choosenColor);
        i++;
      }

      i = 0;
      //loop that gets user answers and checks with generatedValues and increments points
      while (i < level) {
        //getting input from users
        String str = choosenColor.toString();
        if (generatedvalues.contains(str)) {
          generatedvalues.remove(str);
          points++;
        }
        i++;
      }
      olderColor = null;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Color Game"),
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            new Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.only(top: 16, bottom: 8),
                    child: Text(
                      "Computer",
                      style: new TextStyle(
                        fontSize: 22.0,
                        color: getColorFromHex(colorBlack),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Column(
                        children: <Widget>[
                          Container(
                            padding: const EdgeInsets.only(top: 8),
                            height: 120,
                            width: 120,
                            child: Card(
                              semanticContainer: true,
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              child: InkWell(
                                child: Container(
                                    padding: const EdgeInsets.only(
                                        left: 0, right: 0),
                                    alignment: Alignment.center,
                                    child: Text(
                                      "",
                                      style: new TextStyle(
                                        fontSize: 16.0,
                                        color: getColorFromHex(colorBlack),
                                      ),
                                    )),
                                onTap: () {},
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0.0),
                              ),
                              color: Colors.red,
                              elevation: 5,
                              margin: EdgeInsets.all(0),
                            ),
                            decoration: decoration0,
                          ),
                          Container(
                            padding: const EdgeInsets.only(top: 0, bottom: 8),
                            height: 120,
                            width: 120,
                            child: Card(
                              semanticContainer: true,
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              child: InkWell(
                                child: Container(
                                    padding: const EdgeInsets.only(
                                        left: 0, right: 0),
                                    alignment: Alignment.center,
                                    child: Text(
                                      "",
                                      style: new TextStyle(
                                        fontSize: 16.0,
                                        color: getColorFromHex(colorBlack),
                                      ),
                                    )),
                                onTap: () {},
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0.0),
                              ),
                              elevation: 5,
                              color: Colors.yellow,
                              margin: EdgeInsets.all(0),
                            ),
                            decoration: decoration0,
                          ),
                        ],
                      ),
                      new Column(
                        children: <Widget>[
                          Container(
                            padding: const EdgeInsets.only(top: 8),
                            height: 120,
                            width: 120,
                            child: Card(
                              semanticContainer: true,
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              child: InkWell(
                                child: Container(
                                    padding: const EdgeInsets.only(
                                        left: 0, right: 0),
                                    alignment: Alignment.center,
                                    child: Text(
                                      "",
                                      style: new TextStyle(
                                        fontSize: 16.0,
                                        color: getColorFromHex(colorBlack),
                                      ),
                                    )),
                                onTap: () async {},
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0.0),
                              ),
                              elevation: 5,
                              color: Colors.green,
                              margin: EdgeInsets.all(0),
                            ),
                            decoration: decoration0,
                          ),
                          Container(
                            padding: const EdgeInsets.only(top: 0, bottom: 8),
                            height: 120,
                            width: 120,
                            child: Card(
                              semanticContainer: true,
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              child: InkWell(
                                child: Container(
                                    padding: const EdgeInsets.only(
                                        left: 0, right: 0),
                                    alignment: Alignment.center,
                                    child: Text(
                                      "",
                                      style: new TextStyle(
                                        fontSize: 16.0,
                                        color: getColorFromHex(colorBlack),
                                      ),
                                    )),
                                onTap: () async {},
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0.0),
                              ),
                              elevation: 5,
                              color: Colors.blue,
                              margin: EdgeInsets.all(0),
                            ),
                            decoration: decoration0,
                          )
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            new Container(
              padding: const EdgeInsets.only(top: 400, bottom: 24),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Text(
                      "Player",
                      style: new TextStyle(
                        fontSize: 22.0,
                        color: getColorFromHex(colorBlack),
                      ),
                    ),
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Column(
                        children: <Widget>[
                          Container(
                            padding: const EdgeInsets.only(top: 8),
                            height: 120,
                            width: 120,
                            child: Card(
                              semanticContainer: true,
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              child: InkWell(
                                child: Container(
                                    padding: const EdgeInsets.only(
                                        left: 0, right: 0),
                                    alignment: Alignment.center,
                                    child: Text(
                                      "",
                                      style: new TextStyle(
                                        fontSize: 16.0,
                                        color: getColorFromHex(colorBlack),
                                      ),
                                    )),
                                onTap: () {
                                  firestoreInstance
                                      .collection("Game")
                                      .add({"Points": "1"}).then((value) {
                                    print(value.documentID);
                                  });
                                },
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0.0),
                              ),
                              color: Colors.red,
                              elevation: 5,
                              margin: EdgeInsets.all(0),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(top: 0, bottom: 8),
                            height: 120,
                            width: 120,
                            child: Card(
                              semanticContainer: true,
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              child: InkWell(
                                child: Container(
                                    padding: const EdgeInsets.only(
                                        left: 0, right: 0),
                                    alignment: Alignment.center,
                                    child: Text(
                                      "",
                                      style: new TextStyle(
                                        fontSize: 16.0,
                                        color: getColorFromHex(colorBlack),
                                      ),
                                    )),
                                onTap: () {
                                  createRecord(1.toString(), 2.toString());
                                },
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0.0),
                              ),
                              elevation: 5,
                              color: Colors.yellow,
                              margin: EdgeInsets.all(0),
                            ),
                          ),
                        ],
                      ),
                      new Column(
                        children: <Widget>[
                          Container(
                            padding: const EdgeInsets.only(top: 8),
                            height: 120,
                            width: 120,
                            child: Card(
                              semanticContainer: true,
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              child: InkWell(
                                child: Container(
                                    padding: const EdgeInsets.only(
                                        left: 0, right: 0),
                                    alignment: Alignment.center,
                                    child: Text(
                                      "",
                                      style: new TextStyle(
                                        fontSize: 16.0,
                                        color: getColorFromHex(colorBlack),
                                      ),
                                    )),
                                onTap: () async {},
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0.0),
                              ),
                              elevation: 5,
                              color: Colors.green,
                              margin: EdgeInsets.all(0),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(top: 0, bottom: 8),
                            height: 120,
                            width: 120,
                            child: Card(
                              semanticContainer: true,
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              child: InkWell(
                                child: Container(
                                    padding: const EdgeInsets.only(
                                        left: 0, right: 0),
                                    alignment: Alignment.center,
                                    child: Text(
                                      "",
                                      style: new TextStyle(
                                        fontSize: 16.0,
                                        color: getColorFromHex(colorBlack),
                                      ),
                                    )),
                                onTap: () async {},
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0.0),
                              ),
                              elevation: 5,
                              color: Colors.blue,
                              margin: EdgeInsets.all(0),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

void createRecord(String level, String score) async {
  DocumentReference ref = await firestoreInstance
      .collection("Game")
      .add({'Level': level, 'Score': score});
  print(ref.documentID);
}
